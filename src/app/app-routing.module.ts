import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagetwoComponent } from './pagetwo/pagetwo.component';
import { RegisterComponent } from './register/register.component';
import { OtpComponent } from './otp/otp.component';
import { OnboardComponent } from './onboard/onboard.component';
import { TeamComponent } from './team/team.component';
import { PagethreeComponent } from './pagethree/pagethree.component';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path:'',
    component:DashboardComponent
  },
  {
    path:'register',
    component: RegisterComponent
  },
  {
    path:'otp',
    component:OtpComponent
  },
  {
    path:'onboard',
    component: OnboardComponent
  },
  {
    path:'team',
    component: TeamComponent
  },
  {
    path:'pagetwo',
    component: PagetwoComponent
  },
  {
    path:'pagethree',
    component: PagethreeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
