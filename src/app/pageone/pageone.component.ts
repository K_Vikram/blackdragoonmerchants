import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-pageone',
  templateUrl: './pageone.component.html',
  styleUrls: ['./pageone.component.css']
})
export class PageoneComponent implements OnInit {
  tabs: any[] =[];
  disabled: boolean = false;
  constructor(private route: Router) { 
    this.tabs.push(
      {title: 'SignUp', path: 'register', class: 'active', disable: false},
      {title: 'OTP', path: 'otp', class: 'disabled', disable: true},
      {title: 'Onboard', path: 'onboard', class: 'disabled', disable: true},
      {title: 'Team', path: 'team', class: 'disabled', disable: true},
    );
  }

  ngOnInit(): void {
  }
  goActive(counter) {
    console.log("src ", counter);
    this.tabs[counter].class = 'active';
    this.tabs[counter].disable = false;
    _.forEach(this.tabs, (tab, index) => {
      if(index < counter) {
        tab.class = 'focus';
        tab.disable = false;
      }
    })
  }


}
