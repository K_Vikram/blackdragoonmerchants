import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  team = [
    { "id":1,"Name":"BlackDragoon","Email":"black@gmail.com"},
    { "id":2,"Name":"ThunderDragoon","Email":"thunder@gmail.com"},
    { "id":3,"Name":"StataDragoon","Email":"stata@gmail.com"},
    { "id":4,"Name":"FireDragoon","Email":"fire@gmail.com"}
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
