import { Component, OnInit, Inject } from '@angular/core';
import { JsonPipe } from '@angular/common';
@Component({
  selector: 'app-pagetwo',
  templateUrl: './pagetwo.component.html',
  styleUrls: ['./pagetwo.component.css']
})
export class PagetwoComponent implements OnInit {
  public data : any  = [];
  public local : any = [];
  constructor() {
  }
  
  ngOnInit(): void {
  }
  getData(){
    this.data = JSON.parse(localStorage.getItem('Users'));
    this.local = JSON.parse(localStorage.getItem('loc'));
  }

  
}
