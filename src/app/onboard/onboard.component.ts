import { Component, OnInit } from '@angular/core';
import { FormGroup,Validators,FormControl } from '@angular/forms';
@Component({
  selector: 'app-onboard',
  templateUrl: './onboard.component.html',
  styleUrls: ['./onboard.component.css']
})
export class OnboardComponent implements OnInit {
  public data;
  form = new FormGroup({
    name : new FormControl('',Validators.required),
    city : new FormControl('',Validators.required),
    occupation : new FormControl('',Validators.required)
  });
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(){
    let payload = {
      city : this.form.get('city').value,
      occupation : this.form.get('occupation').value,
      
    };
    this.data = JSON.stringify(payload);
    localStorage.setItem('loc',this.data);
  }
}
