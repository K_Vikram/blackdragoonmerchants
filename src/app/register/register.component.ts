import { Component, OnInit } from '@angular/core';
import { FormGroup,Validators,FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user : any = {};
  public data;
  form = new FormGroup({
    fName : new FormControl('',Validators.required),
    lName : new FormControl('',Validators.required),
    email : new FormControl('',Validators.required),
    phone : new FormControl('',[Validators.required,Validators.minLength(10)]),
    password : new FormControl('',[Validators.required,Validators.minLength(6)]),
    cPassword : new FormControl('',[Validators.required,Validators.minLength(6)]),
    quesType : new FormControl('',Validators.required)
  });
 
  constructor(
    private route: ActivatedRoute,
    private router : Router

  ) { }

  ngOnInit(): void {
    
  }
  // goNext() {
  //   if(this.form.valid){
  //     this.router.navigate(['../otp'],{relativeTo: this.route});
  //   }
  // }
  onSubmit(){
    console.log(this.form.value);
    let payload = {
      fName : this.form.get('fName').value,
      lName : this.form.get('lName').value,
      email : this.form.get('email').value,
      mobileNo : this.form.get('phone').value
    };
    this.data = JSON.stringify(payload);
    localStorage.setItem('Users',this.data);
  }
}
